## Note that this project is intended to be only used by GitLab team-members. If you are member of wider community, consider opening an issue in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues) or check our [support options](https://about.gitlab.com/support).

This project is a proof of concept for using GitLab issues instead of Slack threads. Main reason for doing that is the new [retention policy for Slack](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues/25).

Use [issues](https://gitlab.com/gitlab-com/support/questions/issues) for asking questions. If they have content not suitable for public, [make them confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#making-an-issue-confidential).

### TL;DR: What you ask in Slack disappears after 3 months, what you ask here stays (hopefully) forever.